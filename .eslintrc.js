module.exports = {
    "env": {
        "es6": true,
        // "browser": true,
        "node": true,
        // "jest/globals": true,
    },
    "globals": {
        "__DEV__": true,   // read/write
        "__TEST__": false, // read-only
        "__PROD__": false, // read-only
    },
    "plugins": [
        // "jest",
        // "html",
        // "vue",
    ],
    "parserOptions": {
        // "parser": "babel-eslint",
        "ecmaVersion": 2019,
        "sourceType": "script",
    },
    "extends": [
        "eslint:recommended",
        // "plugin:vue/recommended",
    ],
    "rules": {
        "indent": ["error", 4, {
            "SwitchCase": 1,
            "MemberExpression": "off",
            "flatTernaryExpressions": true,
        }],
        "no-unused-vars": ["warn", {
            "args": "all",
            //Unused args should begin and end with an underscore
            //Also allow common libraries to be required even if not used
            "argsIgnorePattern": "(^.+_$)",
            "varsIgnorePattern": "(^.+_$)",
        }],
        "comma-spacing": ["error", {
            "before": false,
            "after": true,
        }],
        "comma-dangle": ["error", "always-multiline"],
        "eol-last": "error",
        "eqeqeq": "error",
        "linebreak-style": ["error",
            "unix",
        ],
        "no-alert": "error",
        "no-console": "off",
        "no-else-return": "error",
        "no-empty": ["error", { "allowEmptyCatch": true }],
        "no-empty-function": "warn",
        "no-eval": "error",
        "no-implied-eval": "error",
        "no-nested-ternary": "error",
        "no-new-func": "error",
        "no-restricted-globals": ["error",
            "event",
            "error",
            "self",
        ],
        "no-return-assign": "error",
        "no-self-compare": "error",
        "no-throw-literal": "error",
        "no-unneeded-ternary": "error",
        "no-var": "error",
        "no-void": "error",
        "no-with": "error",
        "prefer-spread": "error", //Use func(...array), not func.apply(this, array);
        "prefer-rest-params": "error", //Use `function (...args)`, not `arguments`
        "radix": ["error",
            "as-needed",
        ],
        "semi": ["error",
            "always",
        ],
        "semi-spacing": ["error", {
            "before": false,
            "after": true,
        }],
        // "space-infix-ops": "error", // off because `foo+= "bar"`
        "space-unary-ops": ["error", {
            "words": true,
            "nonwords": false,
        }],
        "spaced-comment": ["error", "always", {
            //Allows for comment blocks like //----, //====, //####, etc.
            "exceptions": [
                "~", "!", "@", "#", "$", "%", "^",
                "&", "*", "(", ")", "-", "_", "=",
                "+", "[", "]", "{", "}", "<", ">",
                "?", ";", ":", "'", '"', ",", ".",
                "/", "\\",
            ],
        }],
        "no-warning-comments": ["warn", {
            "terms": ["todo", "fixme"],
            "location": "start",
        }],
        // Jest Rules
        // "jest/no-disabled-tests": "warn",
        // "jest/no-focused-tests": "error",
        // "jest/no-identical-title": "error",
        // "jest/prefer-to-have-length": "warn",
        // "jest/valid-expect": "error",
    },
};
