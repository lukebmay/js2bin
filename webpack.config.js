/* eslint-env node */

const webpack = require("webpack");
const path = require("path");
const fs = require("fs");

// Environment variables
let environment = (process.env.NODE_ENV || "").toLowerCase();
if (environment === "production" || environment === "prod")
    environment = "prod";
else if (environment === "testing" || environment === "test")
    environment = "test";
else
    environment = "dev";
const isDev  = environment === "dev";
const isTest = environment === "test";
const isProd = environment === "prod";

const commandsDir = path.join(__dirname, "src", "commands");
const commandPaths = fs.readdirSync(commandsDir)
.filter(function (filename) {
    const abs = path.join(commandsDir, filename);
    const split = filename.split(".");
    const hasJsExt = split[split.length-1] === "js";
    if (!hasJsExt) {
        return false;
    }
    const stats = fs.statSync(abs);
    return stats.isFile();
});
const commandPathsObj = {};
commandPaths.forEach(function (p) {
    const abs = path.join(commandsDir, p);
    commandPathsObj[p] = abs;
});


const webpacker = function(){

    const config = {
        entry: commandPathsObj,
        context: commandsDir,
        output: {
            path: path.join(__dirname, "dist"),
            filename: "[name]",
        },

        mode: environment === "prod" ? "production" : "development", // can also be "none"

        resolve: {
            extensions: [".js", ".md", ".json5", ".json"],
        },

        module: {
            rules: [
                {
                    test: /\.js$/,
                    loader: "shebang-loader",
                    exclude: /(node_modules)/,
                },
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                __PROD__: isProd,
                __TEST__: isTest,
                __DEV__ : isDev,
            }),
        ],
    };

    return config;
};


module.exports = webpacker;
