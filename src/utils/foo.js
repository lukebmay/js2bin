console.log("foo loading...");

function logFoo() {
    console.log("logFoo()...");
}
function logBar() {
    console.log("logBar()...");
}

module.exports = {
    logFoo,
    logBar,
};
