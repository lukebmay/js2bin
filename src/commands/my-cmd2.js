#!/usr/bin/env node

console.log("./my-cmd2.js");

const _ = {
    forEach: require("lodash/forEach"),
};
const foo = require("../utils/foo");
const dep = require("./my-cmd2/dependency");

foo.logFoo();
foo.logBar();

console.log("lodash");
_.forEach(["a", "b", "c"], function (item) {
    console.log(item);
});

dep.dep("WOOT!");
