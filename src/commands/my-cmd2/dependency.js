
console.log("Loading dependencies.js...");

function dep(s) {
    console.log(`dep(): ${s}`);
}

module.exports = {
    dep,
};
