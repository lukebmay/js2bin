const fs = require("fs");
const path = require("path");

const distDir = path.join(__dirname, "../", "dist");

const fileListWithExt = fs.readdirSync(distDir).map(function (filename) {
    return path.join(distDir, filename);
}).filter(function (filename) {
    const split = filename.split(".");
    const hasJsExt = split[split.length-1] === "js";
    if (!hasJsExt) {
        return false;
    }
    const stats = fs.statSync(filename);
    return stats.isFile();
});

const fileListNoExt = fileListWithExt.map(function (filename) {
    return filename.split('.').slice(0, -1).join('.');
});

for (let i=0; i < fileListWithExt.length; i++) {
    const filenameWithExt = fileListWithExt[i];
    const filenameNoExt = fileListNoExt[i];
    fs.renameSync(filenameWithExt, filenameNoExt);
    let fileData = fs.readFileSync(filenameNoExt);
    fileData = "#!/usr/bin/env node\n" + fileData;
    fs.writeFileSync(filenameNoExt, fileData);
    fs.chmodSync(filenameNoExt, "755");
}
